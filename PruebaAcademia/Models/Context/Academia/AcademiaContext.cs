﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PruebaAcademia.Models.Context.Academia
{
    public partial class AcademiaContext : DbContext
    {
        public AcademiaContext(DbContextOptions<AcademiaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Estudiante> Estudiante { get; set; }
        public virtual DbSet<Inscripcion> Inscripcion { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<TipoLicencia> TipoLicencia { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estudiante>(entity =>
            {
                entity.HasKey(e => e.Documento);

                entity.Property(e => e.Documento).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTipoLicenciaNavigation)
                    .WithMany(p => p.Estudiante)
                    .HasForeignKey(d => d.IdTipoLicencia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Estudiante_TipoLicencia");
            });

            modelBuilder.Entity<Inscripcion>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => new { e.DocumentoEstudiante, e.IdModulo })
                    .HasName("IX_Inscripcion")
                    .IsUnique();

                entity.HasOne(d => d.DocumentoEstudianteNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.DocumentoEstudiante)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Inscripcion_Estudiante");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Inscripcion_Modulo");
            });

            modelBuilder.Entity<Modulo>(entity =>
            {
                entity.HasKey(e => e.IdModulo);

                entity.Property(e => e.NombreModulo)
                    .IsRequired()
                    .HasColumnName("modulo")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoLicencia>(entity =>
            {
                entity.HasKey(e => e.IdTipoLicencia);

                entity.Property(e => e.NombreTipoLicencia)
                    .IsRequired()
                    .HasColumnName("TipoLicencia")
                    .HasMaxLength(2);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
