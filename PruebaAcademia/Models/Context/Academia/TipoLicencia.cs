﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PruebaAcademia.Models.Context.Academia
{
    public partial class TipoLicencia
    {
        public TipoLicencia()
        {
            Estudiante = new HashSet<Estudiante>();
        }

        public int IdTipoLicencia { get; set; }
        public string NombreTipoLicencia { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Estudiante> Estudiante { get; set; }
    }
}
