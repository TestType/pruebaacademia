﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PruebaAcademia.Models.Context.Academia
{
    public partial class Estudiante
    {
        public int Documento { get; set; }
        public string Nombre { get; set; }
        public byte Edad { get; set; }
        public int IdTipoLicencia { get; set; }

        public virtual TipoLicencia IdTipoLicenciaNavigation { get; set; }
    }
}
