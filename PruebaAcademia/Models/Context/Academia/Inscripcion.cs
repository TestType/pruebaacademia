﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PruebaAcademia.Models.Context.Academia
{
    public partial class Inscripcion
    {
        public int DocumentoEstudiante { get; set; }
        public int IdModulo { get; set; }
        public int NivelModulo { get; set; }

        public virtual Estudiante DocumentoEstudianteNavigation { get; set; }
        public virtual Modulo IdModuloNavigation { get; set; }
    }
}
