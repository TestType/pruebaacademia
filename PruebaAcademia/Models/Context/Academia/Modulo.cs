﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace PruebaAcademia.Models.Context.Academia
{
    public partial class Modulo
    {
        public int IdModulo { get; set; }
        public string NombreModulo { get; set; }
    }
}
