﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PruebaAcademia.Interfaces;
using PruebaAcademia.Models.Context.Academia;
using System;
using System.Threading.Tasks;

namespace PruebaAcademia.Controllers
{
    [ApiController]
    [Route("[controller]/[action]/{id?}")]
    public class AcademiaController : Controller
    {
        private IAcademiaService Academia { get; }
        private ILogger<AcademiaController> _logger { get; }

        public AcademiaController(ILogger<AcademiaController> logger, IAcademiaService academia) 
        {
            _logger = logger;
            Academia = academia;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Bienvenido a la academia");
        }

        [HttpGet]
        public IActionResult ConsultarModulos() 
        {
            try
            {
                return Ok(Academia.ObtenerListadoModulos());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return BadRequest();
            }
        }

        [HttpGet]
        public IActionResult ConsultarTipoLicencias()
        {
            try
            {
                return Ok(Academia.ObtenerListadoTipoLicencias());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return BadRequest();
            }
        }

        [HttpGet]
        public IActionResult ConsultarListadoEstudiantes()
        {
            try
            {
                return Ok(Academia.ObtenerListadoEstudiantes());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return BadRequest();
            }
        }

        [HttpGet]
        public IActionResult ConsultarDetalleEstudiantes(int id)
        {
            try
            {
                return Ok(Academia.ObtenerDetalleEstudiante(id));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return BadRequest();
            }
        }

        [HttpGet]
        public IActionResult ConsultarModulosInscritosPorEstudiantes(int id)
        {
            try
            {
                return Ok(Academia.ObtenerListadoInscripcionesPorEstudiante(id));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> CrearNuevoEstudiantes([FromBody] Estudiante estudiante)
        {
            try
            {
                return Ok(await Academia.CrearEstudiante(estudiante));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return Ok(false);
            }
        }
    }
}
