﻿using PruebaAcademia.Models.Context.Academia;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaAcademia.Interfaces
{
    public interface IAcademiaService
    {

        /*
         * crear los estudiantes, 
         * mostrar el listado de estos 
         * tener la opción de ver el detalle (información básica, módulos inscritos y clases activas)
         */

        Task<bool> CrearEstudiante(Estudiante nuevoEstudiante);
        IQueryable<Estudiante> ObtenerListadoEstudiantes();
        Estudiante ObtenerDetalleEstudiante(int documentoEstudiante);
        IQueryable<Inscripcion> ObtenerListadoInscripcionesPorEstudiante(int documentoEstudiante);
        IQueryable<TipoLicencia> ObtenerListadoTipoLicencias();
        IQueryable<Modulo> ObtenerListadoModulos();
    }
}
