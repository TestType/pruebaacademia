﻿using PruebaAcademia.Interfaces;
using PruebaAcademia.Models.Context.Academia;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace PruebaAcademia.Services
{
    public class AcademiaService : IAcademiaService
    {
        private ILogger<AcademiaService> _logger { get; }
        private AcademiaContext CtxAcademia { get; }

        public AcademiaService(ILogger<AcademiaService> logger, AcademiaContext ctxAcademia) 
        {
            _logger = logger;
            CtxAcademia = ctxAcademia;
        }

        public async Task<bool> CrearEstudiante(Estudiante nuevoEstudiante)
        {
            _logger.LogInformation($"Ingresa a {nameof(CrearEstudiante)}");

            await CtxAcademia.Estudiante.AddAsync(nuevoEstudiante);
            await CtxAcademia.SaveChangesAsync();
            return true;
        }

        public Estudiante ObtenerDetalleEstudiante(int documentoEstudiante)
        {
            _logger.LogInformation($"Ingresa a {nameof(ObtenerDetalleEstudiante)}");

            return CtxAcademia.Estudiante.Where(x => x.Documento == documentoEstudiante)
                .Include(x => x.IdTipoLicenciaNavigation)
                .FirstOrDefault();
        }

        public IQueryable<Estudiante> ObtenerListadoEstudiantes()
        {
            _logger.LogInformation($"Ingresa a {nameof(ObtenerListadoEstudiantes)}");

            return CtxAcademia.Estudiante.AsQueryable();
        }

        public IQueryable<Inscripcion> ObtenerListadoInscripcionesPorEstudiante(int documentoEstudiante) 
        {
            _logger.LogInformation($"Ingresa a {nameof(ObtenerListadoInscripcionesPorEstudiante)}");

            return CtxAcademia.Inscripcion.Where(x => x.DocumentoEstudiante == documentoEstudiante)
                .Include(x => x.IdModuloNavigation)
                .AsQueryable();
        }

        public IQueryable<Modulo> ObtenerListadoModulos()
        {
            _logger.LogInformation($"Ingresa a {nameof(ObtenerListadoModulos)}");

            return CtxAcademia.Modulo.AsQueryable();
        }

        public IQueryable<TipoLicencia> ObtenerListadoTipoLicencias()
        {
            _logger.LogInformation($"Ingresa a {nameof(ObtenerListadoTipoLicencias)}");

            return CtxAcademia.TipoLicencia.AsQueryable();
        }
    }
}
