using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PruebaAcademia.Interfaces;
using PruebaAcademia.Models.Context.Academia;
using PruebaAcademia.Services;

namespace PruebaAcademia
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<AcademiaContext>(options => options
            .UseSqlServer(Configuration.GetConnectionString("AcademiaDatabase"), sqlServerOptionsAction: sqlOptions => { sqlOptions.EnableRetryOnFailure(); }));

            services.AddScoped<IAcademiaService, AcademiaService>();
            services.AddControllers().AddNewtonsoftJson();
            services.AddSwaggerGen(s => s.SwaggerDoc("v1", new OpenApiInfo { Title = "AcademiaAPI", Version = "v1" }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(s => s.SwaggerEndpoint("/swagger/v1/swagger.json", "AcademiaAPI"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
